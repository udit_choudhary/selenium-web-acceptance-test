Feature: KiwiSaver Retirement Calculator - field information validation
  As a product owner
  I want that while using the KiwiSaver Retirement Calculator all fields in the calculator have got the information icon present
  So that the user is able to get a clear understanding of what needs to be entered in the field

  @information-icon-message
  Scenario: Display information field - Age Limit
    Given user is on "KiwiSaver Retirement Calculator" page
    When user clicks on the information icon beside "current age" input field
    Then user should be provided with "age limit" message below input field
