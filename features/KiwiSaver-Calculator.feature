Feature: KiwiSaver Retirement Calculator projected balance validation
  As a product owner
  I want that the KiwiSaver Retirement Calculator users are able to calculate what their KiwiSaver projected balance would be at retirement
  So that the users are able to plan their investments better.

  Background: User is on KiwiSaver calculator page
    Given user is on "KiwiSaver Retirement Calculator" page

  #Option 1: Declarative way
  #data provided (data table) in feature file
  @calculation-employed
  Scenario: User at 30, Employed, kiwi saver contributor, high risk profile
    Given the user fills the form with following information
    |Current age            |30       |
    |Employment status      |Employed |
    |salary                 |82000    |
    |kiwisaver contribution | 4       |
    |PIR                    | 17.5    |
    |Risk profile           | high    |
    When the user views KiwiSaver retirement projection
    Then the user should be presented with calculated projected balance at retirement

  @calculation-self-employed
  Scenario: User at 45, Self-employed,fortnight voluntary contribution and medium risk profile with saving goals
    Given the user fills the form with following information
      |Current age            |45       |
      |Employment status      |self-employed |
      |PIR                    | 10.5    |
      |kiwisaver balance      | 100000  |
      |Voluntary contributions| 90      |
      |Voluntary contributions Frequency| Fortnight|
      |Risk profile           | medium  |
      |saving goal            | 290000  |
    When the user views KiwiSaver retirement projection
    Then the user should be presented with calculated projected balance at retirement

  #Option 2: Declarative way
  #data is taken from JSON profile
  @calculation-not-employed
  Scenario: Unemployed user,age 55, low annual contribution and medium risk profile with saving goals
    Given the user profile is "Tom Keen"
    And user fills the form with correct values
    When the user views KiwiSaver retirement projection
    Then the user should be presented with calculated projected balance at retirement
