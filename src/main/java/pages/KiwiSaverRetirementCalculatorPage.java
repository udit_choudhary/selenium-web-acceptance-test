package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Iterator;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static utils.Helper.*;

public class KiwiSaverRetirementCalculatorPage extends BasePage {

    @FindBy(xpath = "//div[@help-id='CurrentAge']//i")
    private WebElement currentAgeInfoIcon;

    @FindBy(xpath = "//div[@help-id='CurrentAge']//p")
    private WebElement currentAgeInfoMsg;

    @FindBy(xpath = "//div[@help-id='CurrentAge']//input")
    private WebElement currentAgeTextField;

    @FindBy(xpath = "//div[@help-id='AnnualIncome']//input")
    private WebElement salaryTextField;

    @FindBy(xpath = "//div[@help-id='EmploymentStatus']//div[@class='control-well']")
    private WebElement employmentStatusDropdown;

    @FindBy(xpath = "//div[@help-id='PIRRate']//div[@class='control-well']")
    private WebElement pirDropdown;

    @FindBy(xpath = "//div[@help-id='RiskProfile']")
    private WebElement riskProfile;

    @FindBy(xpath = "//div[@class='field-group-set']/button")
    private WebElement completeTheFormButton;

    @FindBy(className = "result-value")
    private WebElement projectedValue;

    @FindBy(xpath = "//div[@help-id='KiwiSaverBalance']//input")
    private WebElement currentKiwiSaverBalanceTextField;

    @FindBy(xpath = "//div[@help-id='SavingsGoal']//input")
    private WebElement SavingsGoalTextField;

    @FindBy(xpath = "//div[@help-id='VoluntaryContributions']//input")
    private WebElement voluntaryContTextField;

    @FindBy(xpath = "//div[@help-id='VoluntaryContributions']//span[contains(.,'Frequency')]")
    private WebElement voluntaryContDropDown;

    public void tapInfoIcon(String inputField){
        switchToCalFrame();
        switch (inputField.toLowerCase()){
            case "current age":
                mouseHoverClick(elementVisibilityWait(currentAgeInfoIcon));
                break;
            default:
                fail("KiwiSaverRetirementCalculatorPage_tapInfoIcon: Invalid Input field");
        }
    }

    public void validateMessageInfo(String messageInfo) {
        String actual, expected;
        switch (messageInfo.toLowerCase()){
            case "age limit":
                expected = getConstantValue("AGE_LIMIT_INFO");
                actual = elementVisibilityWait(currentAgeInfoMsg).getText();
                assertTrue("Fail: validateMessageInfo - AGE_LIMIT_INFO\nExpected:"+expected+"\nActual:"+actual,
                        expected.equalsIgnoreCase(actual));
                break;
            default:
                fail("KiwiSaverRetirementCalculatorPage_validateMessageInfo: Invalid msg info field");;
        }
    }

    public void fillForm(Map<String, String> userInfo) {
        switchToCalFrame();
        Iterator iterator= userInfo.entrySet().iterator();
        while(iterator.hasNext())
        {
            Map.Entry entry =(Map.Entry)iterator.next();
            String key = entry.getKey().toString();
            String value = String.valueOf(userInfo.get(key));
            fillField(key,value);
        }
    }

    private void fillField(String key, String value) {
        switch (key.toLowerCase()){
            case "current age":
                currentAgeTextField.click();
                currentAgeTextField.sendKeys(value);
                break;
            case "employment status":
                elementVisibilityWait(employmentStatusDropdown).click();
                driver.findElement(By.xpath("//li[@value='"+value.toLowerCase()+"']")).click();
                break;
            case "salary":
                elementVisibilityWait(salaryTextField).sendKeys(value);
                break;
            case "kiwisaver contribution":
                driver.findElement(By.xpath("//input[@value='"+value+"']")).click();
                break;
            case "pir":
                elementVisibilityWait(pirDropdown).click();
                driver.findElement(By.xpath("//li[@value='"+value.toLowerCase()+"']")).click();
                break;
            case "kiwisaver balance":
                elementVisibilityWait(currentKiwiSaverBalanceTextField).sendKeys(value);
                break;
            case "voluntary contributions":
                elementVisibilityWait(voluntaryContTextField).clear();
                voluntaryContTextField.sendKeys(value);
                break;
            case "voluntary contributions frequency":
                elementVisibilityWait(voluntaryContDropDown).click();
                driver.findElement(By.xpath("//li[@value='"+value.toLowerCase()+"']")).click();
                break;
            case "risk profile":
                elementVisibilityWait(riskProfile).findElement(By.xpath("//input[@value='"+value+"']")).click();
                break;
            case "saving goal":
                elementVisibilityWait(SavingsGoalTextField).sendKeys(value);
                break;
            default:
                fail("fillField: Invalid key passed : "+key);
        }
    }

    public void viewProjection() {
        completeTheFormButton.click();
    }

    public void calculatedProjectionPresented() {
        String text = elementVisibilityWait(projectedValue).getText().substring(2).replace(",","");
        assertTrue("calculatedProjectionPresented: \nText: "+text,text.matches("-?\\d+"));
    }

    public static void switchToCalFrame(){
        driver.switchTo().frame(0);
        //wait for frame content to load
        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
