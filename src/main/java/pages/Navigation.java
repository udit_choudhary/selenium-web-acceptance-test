package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static utils.Helper.elementVisibilityWait;
import static utils.Helper.mouseHover;

public class Navigation extends BasePage{

    @FindBy(id = "ubermenu-section-link-kiwisaver-ps")
    private WebElement menu_kiwiSaver;

    @FindBy(id = "ubermenu-item-cta-kiwisaver-calculators-ps")
    private WebElement menu_kiwiSaver_calculator;

    @FindBy(xpath = "//*[@id='content-ps']/div[2]/div/section/p[4]/a")
    private WebElement btn_kiwiSaver_calculator_getStarted;

    @FindBy(tagName = "h1")
    private WebElement page_heading;

    public void navigateTo(String pageName) {

        switch (pageName.toLowerCase()){
            case "calculators":
                mouseHover(menu_kiwiSaver);
                elementVisibilityWait(menu_kiwiSaver_calculator).click();
                break;
            case "kiwisaver retirement calculator":
                navigateTo("calculators");
                elementVisibilityWait(btn_kiwiSaver_calculator_getStarted).click();
                assertTrue("Failed to navigate to Kiwisaver retirement calculator",
                        elementVisibilityWait(page_heading).getText().contains("KiwiSaver Retirement Calculator"));
                break;
            default:
                fail("navigateTo: Invalid page name passed.");
        }
    }
}
