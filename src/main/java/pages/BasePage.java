package pages;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static utils.Helper.getConfigValue;

public class BasePage {

    public static WebDriver driver;
    public static Navigation navigation;
    public static KiwiSaverRetirementCalculatorPage kiwiSaverRetirementCalculatorPage;
    public static Map<String, String> userInfo;

    public BasePage(){
        setSystemProperty();
    }

    public void setupDriver(){
        initiateDriver();
        navigation = PageFactory.initElements(driver, Navigation.class);
        kiwiSaverRetirementCalculatorPage = PageFactory.initElements(driver, KiwiSaverRetirementCalculatorPage.class);

    }

    private void initiateDriver(){
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-fullscreen");
        this.driver = new ChromeDriver(chromeOptions);
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String protocol = getConfigValue("protocol");
        String url = getConfigValue("url");
        this.driver.get(protocol+url);
    }
    private void setSystemProperty() {
        String path = System.getProperty("user.dir") +"/drivers/";
        if (System.getProperty("os.name").contains("Win")) {
            System.setProperty("webdriver.chrome.driver", path+"chrome/chromedriver.exe");
        } else if (System.getProperty("os.name").contains("Mac")) {
            System.setProperty("webdriver.chrome.driver", path+"chrome/mac-chromedriver");
        } else {
            System.setProperty("webdriver.chrome.driver", path+"chrome/linux_chromedriver");
        }
    }

    protected Map<String, String> setUserProfile(String profile) {
        String file = System.getProperty("user.dir") +
                "/src/main/resources/userProfiles/"+ profile.replace(" ","_")
                .toLowerCase() + ".json";
        InputStream fileStream = null;
        try {
            fileStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        };

        Map<String, String> userInfo = null;
        try {
            userInfo = new ObjectMapper().readValue(fileStream, HashMap.class);
        } catch (IOException e) {
            System.out.print("setUserProfile: Failed to read json file");
            e.printStackTrace();
        }
        return userInfo;
    }
}
