package utils;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import pages.BasePage;

import java.io.InputStream;
import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Helper extends BasePage{

    static String getPropertyValue(String property,String key){
        Properties properties = new Properties();

        try {
            String file = property+".properties";
            InputStream inputStream = Helper.class.getClassLoader().getResourceAsStream(file);
            properties.load(inputStream);
        } catch (Throwable t){
            System.out.print("Failed to load config");
            t.printStackTrace();
        }
        return properties.getProperty(key);
    }

    public static String getConfigValue(String key){
        return getPropertyValue("config",key);
    }

    public static String getConstantValue(String key){
        return getPropertyValue("constant",key);
    }

    public static void mouseHover(WebElement el){
        Actions action = new Actions(driver);
        action.moveToElement(el).perform();
    }

    public static void mouseHoverClick(WebElement el){
        Actions action = new Actions(driver);
        action.moveToElement(el).click().perform();
    }

    public static WebElement elementVisibilityWait(WebElement element){
        FluentWait wait = new FluentWait(driver);
        wait.withTimeout(Duration.ofSeconds(60));
        wait.pollingEvery(Duration.ofSeconds(1));
        wait.ignoring(NoSuchElementException.class);
        wait.ignoring(ElementNotVisibleException.class);
        wait.ignoring(TimeoutException.class);
        wait.ignoring(StaleElementReferenceException.class);
        wait.ignoring(InvalidElementStateException.class);
        wait.ignoring(ElementNotInteractableException.class);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        wait.until(ExpectedConditions.visibilityOf(element));
        return element;
    }

}
