package steps;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import pages.BasePage;

public class NavigationStepDefs extends BasePage {

    @Given("^user is on \"([^\"]*)\" page$")
    public void userIsOnPage(String pageName) throws Throwable {
        setupDriver();
        navigation.navigateTo(pageName);
    }

    @After
    public void teardown() {
        if(null != driver) {
            driver.quit();
        }
    }

}
