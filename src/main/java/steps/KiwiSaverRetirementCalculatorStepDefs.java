package steps;

import io.cucumber.java.en.*;
import pages.BasePage;

import java.util.Map;

/**
 * Created by udit on 1/08/19.
 */
public class KiwiSaverRetirementCalculatorStepDefs extends BasePage {

    @When("^user clicks on the information icon beside \"([^\"]*)\" input field$")
    public void userClicksOnTheInformationIconBesideCurrentAgeInputField(String inputField) {
        kiwiSaverRetirementCalculatorPage.tapInfoIcon(inputField);
    }

    @Then("^user should be provided with \"([^\"]*)\" message below input field$")
    public void userShouldBeProvidedWithAgeLimitMessageBelowInputField(String messageInfo) {
        kiwiSaverRetirementCalculatorPage.validateMessageInfo(messageInfo);
    }

    @Given("the user fills the form with following information")
    public void theFollowingUserInformation(Map<String, String> userInfo) {
        kiwiSaverRetirementCalculatorPage.fillForm(userInfo);
    }

    @When("the user views KiwiSaver retirement projection")
    public void theUserViewsKiwiSaverRetirementProjection() {
        kiwiSaverRetirementCalculatorPage.viewProjection();
    }

    @Then("the user should be presented with calculated projected balance at retirement")
    public void theUserShouldBePresentedWithCalculatedProjectedBalanceAtRetirement() {
        kiwiSaverRetirementCalculatorPage.calculatedProjectionPresented();
    }

    @And("user fills the form with correct values")
    public void userFillsTheFormWithCorrectValues() {
        kiwiSaverRetirementCalculatorPage.fillForm(userInfo);
    }
}
