package steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import pages.BasePage;

import java.util.List;
import java.util.Map;

public class StepDefs extends BasePage {

    @Given("the user profile is {string}")
    public void theUserProfileIsTomKeen(String profile) {
        userInfo = setUserProfile(profile);
    }

}
