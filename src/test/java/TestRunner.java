import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
        features="features",
        glue = "steps"
)
@RunWith(Cucumber.class)
public class TestRunner {
}
