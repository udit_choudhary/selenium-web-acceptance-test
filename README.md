# Selenium Web Acceptance Test demo

This project is to showcase web ui test automation framework

## About the repository

This repository contains a maven project to perform automation testing on Westpac kiwisaver calculator

Test scenarios are written in Gherkin format executed by cucumber and JUnit.
Following tools and softwares are used to build the project:

* Java
* Maven
* Cucumber
* Gherkin
* JUnit
* Selenium
* IntelliJ

### Prerequisites

1. Maven path configured
2. Java8 JDK/JRE installed and configured
3. IDE
4. Chrome installed (Version 75.0.3770.142)

### Installing

1. Clone the repository
2. Import it to an IDE
3. Resolve proxies and maven dependencies

## Execute the test

In terminal, go to the project root directory and run:
```
mvn clean test
```
To run a test with specific tag:
```
mvn clean test -Dcucumber.options="--tags @tag-name"
```

## Test Results
To see the results open index.html file places at
```
target/cucumber/index.html
```

## Who do I talk to?

For more information Contact:
**Udit Choudhary** at uditchoudhary@gmail.com

